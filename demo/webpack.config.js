const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");

const ISPROD = process.env.NODE_ENV === 'production';

module.exports = {
    mode: ISPROD ? 'production' : 'development',
    devtool: !ISPROD ? "source-map" : false,
    resolve: {
        extensions: ['*', '.js']
    },
    entry: {
        'wmmodal.demo': './src/js/index.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, "dist"),
        library: 'WMModal'
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            inject: false,
            hash: true,
            template: './src/index.html',
            filename: 'index.html'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
        new BrowserSyncPlugin({
            host: 'wmmodal.com.ua',
            port: 3002,
            server: {
                baseDir: ['dist']
            },
        }),
        // new CopyWebpackPlugin({
        //     patterns: [
        //         {
        //             from: 'src/img',
        //             to: 'img'
        //         }
        //     ]
        // })
    ],
};