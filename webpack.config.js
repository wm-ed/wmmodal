const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const ISPROD = process.env.NODE_ENV === 'production';

module.exports = {
    mode: ISPROD ? 'production' : 'development',
    devtool: !ISPROD ? "source-map" : false,
    resolve: {
        extensions: ['*', '.js']
    },
    entry: {
        'wmmodal.bundle': './src/js/index.js',
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, "dist"),
        libraryTarget: 'umd',
        library: 'WMModal',
    },
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name].css',
        }),
    ],
};