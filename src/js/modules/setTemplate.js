const setClose = `
    <button type="button" aria-label="Close modal" class="wmmodal__close">
        <span class="wmmodal__close-line"></span>
    </button>
`.trim();

export default function () {
    return `
        <div class="wmmodal" data-wmmodal="wmmodal-1">
            <div class="wmmodal__wrapper">
                <div class="wmmodal__container">

                </div>
            </div>
        </div>
    `.trim();
}