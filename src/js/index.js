import '../scss/index.scss';
import setTemplate from './modules/setTemplate';

class WMModal {
    constructor () {

    }
}

WMModal.prototype = {
    setTemplate
}

window.WMModal = WMModal;

export default (() => {
    return new WMModal();
})();